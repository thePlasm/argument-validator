# argument-validator

A scheme program to determine whether an argument in propositional logic is valid.

# usage

The validate function takes a list of propositional logic sentences, with the last being the conclusion and the rest being the premises. It returns true if the argument is valid and false otherwise.
