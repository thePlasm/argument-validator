(define logical-connectives '(and or implies xor xnor not nand nor iff))

(define xnor
    (lambda
        (p q)
        (not (xor p q))))

(define xor
    (lambda
        (p q)
        (and (or p q) (not (and p q)))))

(define nand
    (lambda
        (p q)
        (not (and p q))))

(define nor
    (lambda
        (p q)
        (not (or p q))))

(define iff
    (lambda
        (p q)
        (and (implies p q) (implies q p))))

(define implies
    (lambda
        (p q)
        (or (not p) q)))

(define bool-add
    (lambda
        (lst elt)
        (if
            (or
                (not (pair? lst))
                (null? lst))
            '()
            (cons (xor elt (car lst)) (bool-add (cdr lst) (and elt (car lst)))))))

(define remove-element
    (lambda
        (elt lst)
        (if
            (or
                (not (pair? lst))
                (null? lst))
            '()
            (if
                (eq? elt (car lst))
                (remove-element elt (cdr lst))
                (cons (car lst) (remove-element elt (cdr lst)))))))

(define remove-duplicates
    (lambda
        (lst)
        (if
            (or
                (not (pair? lst))
                (null? lst))
            '()
            (cons (car lst) (remove-duplicates (remove-element (car lst) (cdr lst)))))))

(define last-elem
    (lambda
        (lst)
        (if
            (or
                (not (pair? (cdr lst)))
                (null? (cdr lst)))
            (car lst)
            (last-elem (cdr lst)))))

(define all-but-last
    (lambda
        (lst)
        (if
            (or
                (not (pair? (cdr lst)))
                (null? (cdr lst)))
            '()
            (cons (car lst) (all-but-last (cdr lst))))))

(define find-bindings-form
    (lambda
        (form)
        (if
            (or
                (not (pair? form))
                (null? form))
            (if
                (null? form)
                '()
                (cons form '()))
            (if
                (pair? (car form))
                (append (find-bindings-form (car form)) (find-bindings-form (cdr form)))
                (if
                    (not (member (car form) logical-connectives))
                    (cons (car form) (find-bindings-form (cdr form)))
                    (find-bindings-form (cdr form)))))))

(define find-bindings
    (lambda
        (forms)
        (if
            (or
                (not (pair? forms))
                (null? forms))
            '()
            (append (find-bindings-form (car forms)) (find-bindings (cdr forms))))))

(define false-list
    (lambda
        (bindings)
        (if
            (or
                (not (pair? bindings))
                (null? bindings))
            '()
            (cons #f (false-list (cdr bindings))))))

(define construct-let-recur
    (lambda
        (bindings values)
        (if
            (or
                (not (pair? bindings))
                (null? bindings)
                (not (pair? values))
                (null? values))
            '()
            (cons (cons (car bindings) (cons (car values) '())) (construct-let-recur (cdr bindings) (cdr values))))))

(define construct-let
    (lambda
        (bindings values)
        (cons 'let (cons (construct-let-recur bindings values) '()))))

(define construct-expr
    (lambda
        (forms bindings values)
        (append
            (construct-let bindings values)
            (cons
                (cons
                    'implies
                    (cons
                        (cons
                            'and
                            (all-but-last forms))
                        (cons (last-elem forms) '())))
                '()))))

(define verify-recur
    (lambda
        (argument bindings bools start)
        (if
            (and (not (eval (cons 'or bools) user-initial-environment)) (not start))
            #t
            (and
                (eval (construct-expr argument bindings bools) user-initial-environment)
                (verify-recur argument bindings (bool-add bools #t) (and start #f))))))

(define verify
    (lambda
        (argument)
        (let
            ((bindings (remove-duplicates (find-bindings argument))))
            (verify-recur argument bindings (false-list bindings) #t))))

(define validate verify)